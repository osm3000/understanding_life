import random
class random_agent:
    def __init__(self, **hwrgs):
        self.radar_type = hwrgs.get('radar', None)
        self.agent_mind = hwrgs.get('mind', None)
        self.possible_moves = ['L', 'R', 'U', 'D']
        self.agent_position = [5, 5]
        self.food_amount = 0
        self.agent_id = hwrgs.get('agent_id', 555)
        self.location_history = [self.agent_position]
    def step(self, board):
        # Set the old position to zero
        board[self.agent_position[0], self.agent_position[1]] = 0

        # Select a random move
        move = random.choice(self.possible_moves)
        print(move, self.agent_position)
        # Perform the move and update the agent position
        if move == 'U':
            self.agent_position[0] -= 1
        elif move == 'D':
            self.agent_position[0] += 1
        elif move == 'L':
            self.agent_position[1] -= 1
        elif move == 'R':
            self.agent_position[1] += 1

        if self.agent_position[0] < 0:
            self.agent_position[0] = 0
        elif self.agent_position[0] >= board.shape[0] - 1:
            self.agent_position[0] = board.shape[0] - 1
        
        if self.agent_position[1] < 0:
            self.agent_position[1] = 0
        elif self.agent_position[1] >= board.shape[1] - 1:
            self.agent_position[1] = board.shape[1] - 1
        
        # If there is a food on the current step, eat it
        if board[self.agent_position[0], self.agent_position[1]] == 1:
            self.food_amount += 1
        
        board[self.agent_position[0], self.agent_position[1]] = self.agent_id # the food dissapeared
        
        # Update the history of the position
        self.location_history.append(self.agent_position)
