I want to test the following: given limited resources, is greedy better for the society or not?
The setup is:
1. For completely greedy agents, how does it look like for the rest of the society?
2. Generate another parameter/rule, allowing the agent to give his food to a hungary agent close to him

There is a limited time, where the agents have to do every thing.
The agents will have a tiny network of 5 units each, recurrent, serving as the brain. It will decide to:
    - Move in which direction (4)
    - Get the perception of the blocks in front of it only (a cone)
    - The agent size is one dot
    - If another agents is in the vicinity, the current agent can decide to help him or not, only if it has enough food.

------------------
Self technical questions:
- What is an agent? The idea is that I don't want to create too many classes of agents. They all have the same mind, but different perception, position, direction. Thus, I can create only one agent object, that can store the data about all the other agents.
    - I think this is much better
    - If each agent will use RNN, then, while the model is the same, each will have a different memory, thus, each needs to an independent instance of the class
        - However, they can be recycled from one generation to another. No need to create new instances. 
- When updating the environment, each agent much take a step, and somehow avoid a 'racing' condition. Let's say two agents are on he same distance from a food cell, if they both take one step towards food, they will both be in the cell. Who gets the food then?
    - I think I can randomize the order for the agents steps. At each step in the game, the order of the agents is shuffled, thus, giving them all equal opportunity to be the first to change the environment.
        - This is not an elegant solution. I should check on reddit/internet if there is a better solution
- At the moment, the agent has the same orientation (thus, the directions are absolute). Let's keep it like that for now to test the rest of the program. However, remember to transfer this into 'relative directions', where he agent can turn left and right.
