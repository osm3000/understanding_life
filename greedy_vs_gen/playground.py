import numpy as np
import agents

class playground:
    def __init__(self, **hwrgs):
        self.board_dim = hwrgs.get('dim', (10, 10))
        self.food_percent = hwrgs.get('food_percent', 0.8) # a value from zero to one

        self.board = np.zeros(self.board_dim)
        self.nb_agents = hwrgs.get('nb_agents', 2)
        self.agents_list = []
        for i in range(self.nb_agents):
            self.agents_list.append(agents.random_agent(agent_id=(i+1)*111))
    
    def fill_board_with_food(self):
        self.random_food_matrix = np.random.rand(self.board_dim[0], self.board_dim[1]) <= self.food_percent
        self.board[self.random_food_matrix] = 1 # food
        self.board[~self.random_food_matrix] = 0 # empty
    
    def step(self):
        # Move the agent (let them take a step)
        for agent in self.agents_list:
            agent.step(self.board)

    def visualize(self):
        print(self.board)
        print(f'Food left: {self.food_percentage_left()}')
        print("/-*"*50)


    def food_percentage_left(self):
        return np.count_nonzero(self.board == 1) / (self.board_dim[0] * self.board_dim[1])

if __name__ == "__main__":
    demo_playground = playground()
    demo_playground.fill_board_with_food()
    for i in range(100):
        demo_playground.step()
        demo_playground.visualize()
    # print(demo_playground.board)
